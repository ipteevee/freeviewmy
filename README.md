# FreeviewMY



## How to use?

Just copy [this link](https://gitlab.com/ipteevee/freeviewmy/-/raw/main/IPTV-FreeviewMY.m3u8) or type https://cutt.ly/IPTV-FreeviewMY on any supported IPTV players.

## Credits

1. samleong123 on [Github](https://github.com/samleong123)
2. weareblahs on [Github](https://github.com/weareblahs)
